
# Location-Service-API
> API to get location of usefull places.

# Pawwsords are always store in hash

### Prerequisites
Before start development. Please install.

```
node
```
```
npm
```
```
yarn
```
```
mongoDB
```

#### clone repository
```shell
git clone https://sureshshetiar7725@bitbucket.org/sureshshetiar7725/locationservice.git
```

### Files to be created when cloned
#### to create .env file
```shell
cp .env.example .env
```
Add your google maps api key in GOOGLE_API.

#### to create log files
File to be created in config directory
```shell
somefile.log
exceptions.log
```
```shell
config/somefile.log
```

### Run this commands to start server

```shell
yarn
yarn build && cd dist
node index.js
```

Now server has started on port 4047

### Access API Doc
```shell
localhost:4047/docs
```

### Run this commands to test

```shell
yarn test
```

### To see test coverage

```shell
yarn test:coverage
```

## To see details about test coverge
cd coverage/lcov-report/

open index.html file in browser

### To see logging
## Inside dist folder
Check for config folder.
> Inside config directory open exceptions.log file
> here you can see log if some error occured

Check for config folder.
> Inside config directory open somefile.log file
> here you can see log when successfull response was sent to user.

### For Demo Find link in Doc
> https://docs.google.com/document/d/1PU4rmLRoWoY2ILPhVrmHcmAurIjCAQN3MO3m47m6Z2M/edit?usp=sharing
