import express from 'express';
import logger from 'morgan';
import passport from 'passport';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compress from 'compression';
import methodOverride from 'method-override';
import cors from 'cors';
import httpStatus from 'http-status';
import expressWinston from 'express-winston';
import expressValidation from 'express-validation';
import helmet from 'helmet';
import Boom from 'boom';
import winstonInstance from './winston';
import { routes } from '../modules';
import config from './config';

const app = express();

if (config.env === 'development') {
  app.use(logger('dev'));
}

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Use the passport package in our application
app.use(passport.initialize());
app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// enable detailed API logging in dev env
if (config.env === 'development') {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  app.use(expressWinston.logger({
    winstonInstance,
    meta: true, // optional: log meta data about request (defaults to true)
    msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
    colorStatus: true, // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
  }));
}
// pass passport for configuration
require('../config/passport')(passport);

// mount all routes on /api path
app.use('/api', routes);

// mount api doc
app.use('/docs', express.static('../docs'));
app.use(express.static('public'));

app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    const options = Object.assign({
      errors: err.errors,
    });
    const error = Boom.badRequest('validation error', options);
    return next(error);
  }

  return next(err);
});

// log error in winston transports except when executing test suite
if (config.env !== 'test') {
  app.use(expressWinston.errorLogger({
    winstonInstance,
  }));
}

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  if (err.isBoom) {
    const payload = Object.assign(err.output.payload, err.data || { errors: [] });
    return res.status(err.output.statusCode).send(payload);
  }
  return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
    statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    error: config.env === 'development' || config.env === 'test' ? err.stack : '',
  });
});


export default app;
