import { ExtractJwt, Strategy } from 'passport-jwt';

// load up the user model
import User from '../modules/user/user.model';
import config from '../config/database'; // get db config file

module.exports = (passport) => {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
  opts.secretOrKey = config.secret;
  passport.use(new Strategy(opts, (jwt_payload, done) => { // eslint-disable-line camelcase
    User.findOne({ _id: jwt_payload._id })
    .lean()
    .then((user) => {
      if (user) {
        // console.log(JSON.parse(JSON.stringify(user)), 'user');
        return done(null, JSON.parse(JSON.stringify(user)));
      }
      return done(null, false);
    })
    .catch(err => done(err, false));
  }));
};
