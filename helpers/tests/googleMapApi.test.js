import chai, { expect } from 'chai';
import googleMapApi from '../googleMapApi';

chai.config.includeStack = true;

describe('### Google Map Api*', () => {
  it('should have locationData function', () => {
    expect(googleMapApi).to.have.keys('locationData');
  });
  it('The function should should return null', (done) => {
    const lat = '19.1668';
    const long = '73.2368';
    const radius = '1500';
    const type = 'restaurant';
    const keyword = 'dilwala';
    const user = '4ed2b809d7446b9a0e000023';
    const data = googleMapApi.locationData({ lat, long, radius, type, keyword, user });
    expect(data);
    expect(data).to.equal(null);
    done();
  });
});
