import rp from 'request-promise';
import Promise from 'bluebird';

module.exports.locationData = ({ lat, long, radius, type, keyword, user }) => {
  const options = {
    // uri: `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat},${long}&radius=${radius}&type=${type}&keyword=${keyword}`,
    uri: 'https://maps.googleapis.com/maps/api/place/nearbysearch/json',
    qs: {
      location: `${lat},${long}`,
      radius,
      type,
      keyword,
      key: process.env.GOOGLE_API,
    },
    headers: {
      'User-Agent': 'Request-Promise',
    },
    json: true,
  };

  const data = rp(options)
 .then((result) => {
   if (result && result.results && result.results.length !== 0) {
    //  return result.results;
     return Promise.map(result.results, (item) => {
       /* eslint-disable no-param-reassign */
       item.location = {
         type: 'Point',
         coordinates: [item.geometry.location.lng, item.geometry.location.lat],
       };
       item.user = user;
       delete item.icon;
       delete item.id;
       delete item.opening_hours;
       delete item.photos;
       delete item.plus_code;
       delete item.reference;
       delete item.scope;
       return item;
     });
   }
   return null;
 });
  return data;
};
