import express from 'express';
import validator from 'express-validation';
import validation from './auth.validation';
import authController from './auth.controller';

const route = express.Router();// eslint-disable-line new-cap

/**
 * @api {post} /api/authenticate get token.
 * @apiVersion 0.0.1
 * @apiGroup Authentication
 *
 * @apiParam {String} mobile registered mobile number.
 * @apiParam {String} password password.
 *
 * @apiParamExample {json} Body.
 * {
"mobile":"8805664770",
"password":"12345678"
}

 * @apiSuccess {String} mobile mobile number.
 * @apiSuccess {String} password password.
 *
 * @apiSuccessExample {json} Success.
 *
 *    {
    "statusCode": 200,
    "success": true,
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1Y2E1ZDIzYTQxN2YyMjBmNGUxZWI4
    ZmUiLCJ1cGRhdGVkQXQiOiIyMDE5LTA0LTA0VDA5OjU1OjEyLjg2MVoiLCJjcmVhdGVkQXQiOiIyMDE5LTA0LTA
    0VDA5OjQ1OjMwLjgxOFoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxlIjoiODgwNTY2NDc3MCIsImVtYWlsIjoic3
    VyZXNoc2hldGlhcjc3MjVAZ21haWwuY29tIiwiZG9iIjoiMTk5Ni0wNi0wN1QwMDowMDowMC4wMDBaIiwiZ2VuZ
    GVyIjoibWFsZSIsIl9fdiI6MH0.E3tZ8FM3N7toOxMgmpxzJH6iGoU2GSsGvad3XkUxdXI",
    "user": {
        "_id": "5ca5d23a417f220f4e1eb8fe",
        "updatedAt": "2019-04-04T09:55:12.861Z",
        "createdAt": "2019-04-04T09:45:30.818Z",
        "name": "suresh",
        "mobile": "8805664770",
        "email": "sureshshetiar7725@gmail.com",
        "dob": "1996-06-07T00:00:00.000Z",
        "gender": "male",
        "__v": 0
    }
}
 *
 * @apiError {String} mobile mobile number not registered.
 * @apiErrorExample {json} NOT_FOUND.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "success": false,
    "message": "Authentication failed. User not found."
}
 *
 * @apiError {String} password wrong password.
 * @apiErrorExample {json} UNAUTHORIZED.
 * HTTP/1.1 401 UNAUTHORIZED
 * {
    "statusCode": 401,
    "success": false,
    "message": "Authentication failed. Wrong password."
}
 *
 * @apiError {String} Bad-Request validation error.
 * @apiErrorExample {json} Bad_Request.
 * {
    "statusCode": 400,
    "error": "Bad Request",
    "message": "validation error",
    "errors": [
        {
            "field": "mobile",
            "location": "body",
            "messages": [
                "\"mobile\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "password",
            "location": "body",
            "messages": [
                "\"password\" is required"
            ],
            "types": [
                "any.required"
            ]
        }
    ]
}
*
*/

route.post('/',
validator(validation.add),
authController.authenticateUser);

export default route;
