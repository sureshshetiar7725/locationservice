import controller from './auth.controller';
import route from './auth.route';

export default {
  controller,
  route,
};
