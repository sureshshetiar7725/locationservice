import chai, { expect } from 'chai';
import validation from '../auth.validation';

chai.config.includeStack = true;

describe('### auth module **auth.validation.js**', () => {
  it('should has add property with body', () => {
    expect(validation).to.have.keys('add');
    expect(validation.add).to.have.keys('body');
    expect(validation.add.body).to.have.keys('mobile', 'password');
  });
});
