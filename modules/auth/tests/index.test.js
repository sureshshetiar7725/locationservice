import chai, { expect } from 'chai';
import index from '../index';

chai.config.includeStack = true;

describe('### auth module **index.js**', () => {
  it('should has controller, route, response property', () => {
    expect(index).to.have.keys('controller', 'route');
  });
});
