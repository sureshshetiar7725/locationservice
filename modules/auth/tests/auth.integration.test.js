import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../../index';
import User from '../../user/user.model';
import authFixture from '../helpers/auth.fixtures';

chai.config.includeStack = true;

/**
 * root level hooks
 */
after((done) => {
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

// inserting data from fixtures into database.
beforeEach((done) => {
  User.collection.insert(authFixture, done);
});

  // emptying database
afterEach((done) => {
  User.remove({}, () => {
    done();
  });
});

describe('## Auth APIs (integration test)', () => {
  const validCredential = {
    mobile: '8805664770',
    password: '12345',
  };

  const wrongPass = {
    mobile: '8805664770',
    password: '123456',
  };

  const wrongMobile = {
    mobile: '8805664771',
    password: '123456',
  };

  const wrongData = {
    mobile: '88056647df71',
  };

  describe('# POST /api/authenticate', () => {
    it('should get token', (done) => {
      request(app)
        .post('/api/authenticate')
        .send(validCredential)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.success).to.equal(true);
          expect(res.body.token);
          expect(res.body.user).to.be.an('object');
          expect(res.body.user.email).to.equal(validCredential.email);
          done();
        })
        .catch(done);
    });
    it('should not get token as password is invalid', (done) => {
      request(app)
        .post('/api/authenticate')
        .send(wrongPass)
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body.statusCode).to.equal(401);
          expect(res.body.success).to.equal(false);
          expect(res.body.message).to.equal('Authentication failed. Wrong password.');
          done();
        })
        .catch(done);
    });
    it('should not get token as mobile is invalid', (done) => {
      request(app)
        .post('/api/authenticate')
        .send(wrongMobile)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.statusCode).to.equal(404);
          expect(res.body.success).to.equal(false);
          expect(res.body.message).to.equal('Authentication failed. User not found.');
          done();
        })
        .catch(done);
    });
    it('should not get token as there is error', (done) => {
      request(app)
        .post('/api/authenticate')
        .send(wrongData)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body.statusCode).to.equal(400);
          expect(res.body.message).to.equal('validation error');
          done();
        })
        .catch(done);
    });
  });
});
