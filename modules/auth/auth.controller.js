import jwt from 'jwt-simple';
import User from '../user/user.model';
import config from '../../config/database';
import response from '../../helpers/response';

// authentication of users.

exports.authenticateUser = (req, res) => {
  User.findOne({
    mobile: req.body.mobile,
  }, (err, user) => {
    if (!user) {
      response.noData(res, { success: false, message: 'Authentication failed. User not found.' });
    } else {
        // check if password matches
      user.comparePassword(req.body.password,
         (err, isMatch) => { // eslint-disable-line no-shadow
           if (isMatch && !err) {
            // if user is found and password is right create a token
             const token = jwt.encode(user, config.secret);
            // return the information including token as JSON
             response.ok(res, { success: true, token: `${token}`, user });
           } else {
             response.unAuthenticated(res, { success: false, message: 'Authentication failed. Wrong password.' });
           }
         });
    }
  });
};
