import objectId from 'objectid';

module.exports = [
  {
    _id: objectId('5ab0c9d77f9ac31f4c7867d2'),
    password: '$2a$10$2ZYSjm3nNFu94rdlP1QGFOCVGdxdY2C.z.AwZY6GjGb/kWtE453hG',
    mobile: '8805664770',
    __v: 0,
  },
];
