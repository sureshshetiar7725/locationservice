/**
 * Define every validation requires
 * to perform CRUD on user.
 */


import Joi from 'joi';

// Defining base schema.
const userSchema = {
  mobile: Joi.string(),
  password: Joi.string(),
};


export default {
  // adding middleware level validation on api endpoint
  add: {
    body: {
      mobile: userSchema.mobile.required(),
      password: userSchema.password.required(),
    },
  },
};
