
/**
 * Define controller function here.
 */

import User from './user.model';
import response from '../../helpers/response';

// add users.
const addUser = [
  (req, res) => {
    User.add(req.body)
        .then(userCreated =>
          response.created(res, { message: 'user added', data: userCreated }))
          .catch(
            /* istanbul ignore next */
            (error) => {
              console.log('error logged', error);
              response.serverError(res, { message: 'Interval Server Error' });
            });
  },
];

// get users list.
const getUserList = [
  (req, res) => {
    User.getUsers()
        .then((userData) => {
          if (userData && userData.length !== 0) {
            response.ok(res, { message: 'users list found', data: userData });
          } else {
            response.noData(res, { message: 'no data found' });
          }
        })
        .catch(
          /* istanbul ignore next */
          (error) => {
            console.log('error logged', error);
            response.serverError(res, { message: 'Interval Server Error' });
          });
  },
];

// get user by id.
const findById = [
  (req, res) => {
    User.findUserById({ _id: req.params._id })
        .then((userDataById) => {
          if (userDataById) {
            response.ok(res, { message: 'user found', data: userDataById });
          } else {
            response.noData(res, { message: 'no data found' });
          }
        })
        .catch(
          /* istanbul ignore next */
          (error) => {
            console.log('error logged', error);
            response.serverError(res, { message: 'Interval Server Error' });
          });
  },
];

// update user by id.
const updateById = [
  (req, res) => {
    User.findUserByIdUpdate({ _id: req.params._id }, req.body)
      .then((userDataUpdated) => {
        if (userDataUpdated) {
          response.ok(res, { message: 'user updated', data: userDataUpdated });
        } else {
          response.noData(res, { message: 'no data found' });
        }
      }).catch(
        /* istanbul ignore next */
        (error) => {
          console.log('error logged', error);
          response.serverError(res, { message: 'Interval Server Error' });
        });
  },
];

// delete user by id.
const deleteUser = [
  (req, res) => {
    User.deleteUser({ _id: req.params._id })
     .then((userDataDeleted) => {
       if (userDataDeleted) {
         response.ok(res, { message: 'user deleted', data: userDataDeleted });
       } else {
         response.noData(res, { message: 'no data found' });
       }
     }).catch(
       /* istanbul ignore next */
       (error) => {
         console.log('error logged', error);
         response.serverError(res, { message: 'Interval Server Error' });
       });
  },
];

export default {
  addUser,
  getUserList,
  findById,
  updateById,
  deleteUser,
};
