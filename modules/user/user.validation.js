
/**
 * Define every validation requires
 * to perform CRUD on user.
 */


import Joi from 'joi';

// Defining base schema.
const userSchema = {
  name: Joi.string(),
  mobile: Joi.string().regex(/^[789]\d{9}$/),
  email: Joi.string(),
  dob: Joi.date(),
  gender: Joi.string(),
  password: Joi.string(),
};


export default {
  // adding middleware level validation on api endpoint
  add: {
    body: {
      name: userSchema.name.required(),
      mobile: userSchema.mobile.required(),
      email: userSchema.email.required(),
      password: userSchema.password.required(),
      dob: userSchema.dob.required(),
      gender: userSchema.gender.required(),
    },
  },
  update: {
    body: {
      name: userSchema.name,
      mobile: userSchema.mobile,
      email: userSchema.email,
      password: userSchema.password,
      dob: userSchema.dob,
      gender: userSchema.gender,
    },
  },
};
