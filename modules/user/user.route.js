/**
 * Define your module's express routes here.
 */

import express from 'express';
import passport from 'passport';
import validator from 'express-validation';
import validation from './user.validation';
import controller from './user.controller';

const routes = express.Router(); // eslint-disable-line new-cap

/**
 * @api {post} /api/user add user.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/user
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 * @apiVersion 0.0.1
 * @apiName addUser
 * @apiGroup User
 *
 * @apiParam {String} name name.
 * @apiParam {String} mobile mobile number unique.
 * @apiParam {String} email email unique.
 * @apiParam {String} password password.
 * @apiParam {String} gender gender.
 * @apiParam {String} dob dob.
 *
 * @apiParamExample {json} Body.
 * {
  "name": "suresh",
  "mobile": "8805664770",
  "password": "12345678",
  "email": "sureshshetiar7725@gmail.com",
  "dob": "1996-06-07",
  "gender": "male"
}

 * @apiSuccess {String} name name.
 * @apiSuccess {String} mobile mobile number unique.
 * @apiSuccess {String} email email unique.
 * @apiSuccess {String} gender gender.
 * @apiSuccess {String} dob dob.
 * @apiSuccessExample {json} Success.
 *
 *    {
    "statusCode": 201,
    "message": "user added",
    "data": {
        "__v": 0,
        "updatedAt": "2019-04-04T09:45:30.818Z",
        "createdAt": "2019-04-04T09:45:30.818Z",
        "name": "suresh",
        "mobile": "8805664770",
        "email": "sureshshetiar7725@gmail.com",
        "dob": "1996-06-07T00:00:00.000Z",
        "gender": "male",
        "_id": "5ca5d23a417f220f4e1eb8fe"
    }
}
 *
 * @apiErrorExample {json} Bad-Request.
 * {
    "statusCode": 400,
    "error": "Bad Request",
    "message": "validation error",
    "errors": [
        {
            "field": "name",
            "location": "body",
            "messages": [
                "\"name\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "mobile",
            "location": "body",
            "messages": [
                "\"mobile\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "email",
            "location": "body",
            "messages": [
                "\"email\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "password",
            "location": "body",
            "messages": [
                "\"password\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "dob",
            "location": "body",
            "messages": [
                "\"dob\" is required"
            ],
            "types": [
                "any.required"
            ]
        },
        {
            "field": "gender",
            "location": "body",
            "messages": [
                "\"gender\" is required"
            ],
            "types": [
                "any.required"
            ]
        }
    ]
}
 *
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.post('/',
passport.authenticate('jwt', { session: false }),
validator(validation.add),
controller.addUser);

/**
 * @api {get} /api/user get users list.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/user
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName getUsers
 * @apiGroup User
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} mobile mobile number unique.
 * @apiSuccess {String} email email unique.
 * @apiSuccess {String} gender gender.
 * @apiSuccess {String} dob dob.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "users list found",
    "data": [
        {
            "_id": "5ca5d23a417f220f4e1eb8fe",
            "updatedAt": "2019-04-04T09:45:30.818Z",
            "createdAt": "2019-04-04T09:45:30.818Z",
            "name": "suresh",
            "mobile": "8805664770",
            "email": "sureshshetiar7725@gmail.com",
            "dob": "1996-06-07T00:00:00.000Z",
            "gender": "male",
            "__v": 0
        }
    ]
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.get('/',
passport.authenticate('jwt', { session: false }),
controller.getUserList);

/**
 * @api {get} /api/user/:_id get user by id.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/user/5ca5d23a417f220f4e1eb8fe
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName getById
 * @apiGroup User
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} mobile mobile number unique.
 * @apiSuccess {String} email email unique.
 * @apiSuccess {String} gender gender.
 * @apiSuccess {String} dob dob.
 * @apiSuccessExample {json} Success.
 *
 *  {
    "statusCode": 200,
    "message": "user found",
    "data": {
        "_id": "5ca5d23a417f220f4e1eb8fe",
        "updatedAt": "2019-04-04T09:45:30.818Z",
        "createdAt": "2019-04-04T09:45:30.818Z",
        "name": "suresh",
        "mobile": "8805664770",
        "email": "sureshshetiar7725@gmail.com",
        "dob": "1996-06-07T00:00:00.000Z",
        "gender": "male",
        "__v": 0
    }
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.get('/:_id',
passport.authenticate('jwt', { session: false }),
controller.findById);

/**
 * @api {put} /api/user/:_id update user.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/user/5ca5d23a417f220f4e1eb8fe
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName updateById
 * @apiGroup User
 *
 * @apiParam {String} name name.
 * @apiParam {String} mobile mobile number unique.
 * @apiParam {String} email email unique.
 * @apiParam {String} password password.
 * @apiParam {String} gender gender.
 * @apiParam {String} dob dob.
 *
 * @apiParamExample {json} Body.
 * {
  "name": "suresh",
  "mobile": "8805664770",
  "password": "12345678",
  "email": "sureshshetiar7725@gmail.com",
  "dob": "1996-06-07",
  "gender": "male"
}
 * @apiSuccess {String} name name.
 * @apiSuccess {String} mobile mobile number unique.
 * @apiSuccess {String} email email unique.
 * @apiSuccess {String} gender gender.
 * @apiSuccess {String} dob dob.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "user updated",
    "data": {
        "_id": "5ca5d23a417f220f4e1eb8fe",
        "updatedAt": "2019-04-04T09:55:12.861Z",
        "createdAt": "2019-04-04T09:45:30.818Z",
        "name": "suresh",
        "mobile": "8805664770",
        "email": "sureshshetiar7725@gmail.com",
        "dob": "1996-06-07T00:00:00.000Z",
        "gender": "male",
        "__v": 0
    }
}
 *
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.put('/:_id',
passport.authenticate('jwt', { session: false }),
validator(validation.update),
controller.updateById);

/**
 * @api {delete} /api/user/:_id delete user by id.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/user/5ca5d23a417f220f4e1eb8fe
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName deleteById
 * @apiGroup User
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} mobile mobile number unique.
 * @apiSuccess {String} email email unique.
 * @apiSuccess {String} gender gender.
 * @apiSuccess {String} dob dob.
 * @apiSuccessExample {json} Success.
 *
 *    {
    "statusCode": 200,
    "message": "user deleted",
    "data": {
        "_id": "5ca5d23a417f220f4e1eb8fe",
        "updatedAt": "2019-04-04T09:55:12.861Z",
        "createdAt": "2019-04-04T09:45:30.818Z",
        "name": "suresh",
        "mobile": "8805664770",
        "email": "sureshshetiar7725@gmail.com",
        "dob": "1996-06-07T00:00:00.000Z",
        "gender": "male",
        "__v": 0
    }
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.delete('/:_id',
passport.authenticate('jwt', { session: false }),
controller.deleteUser);

export default routes;
