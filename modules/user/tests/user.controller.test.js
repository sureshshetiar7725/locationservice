import chai, { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'proxyquire';
import userModel from '../user.model';

// stubbing user dependency from user.controller.
const UserController = proxyquire('../user.controller', { User: userModel });


chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### user controller **user.controller.js**', () => {
  it('should have addUser property', () => {
    expect(UserController).to.be.a('object');
    expect(UserController.addUser).to.be.a('array');
  });
  it('should have getUserList property', () => {
    expect(UserController).to.be.a('object');
    expect(UserController.getUserList).to.be.a('array');
  });
  it('should have findById property', () => {
    expect(UserController).to.be.a('object');
    expect(UserController.findById).to.be.a('array');
  });
  it('should have updateById property', () => {
    expect(UserController).to.be.a('object');
    expect(UserController.updateById).to.be.a('array');
  });
  it('should have deleteUser property', () => {
    expect(UserController).to.be.a('object');
    expect(UserController.deleteUser).to.be.a('array');
  });
});
