import chai, { expect } from 'chai';
import validation from '../user.validation';

chai.config.includeStack = true;

describe('### user module **user.validation.js**', () => {
  it('should have add property with body', () => {
    expect(validation).to.have.keys('add', 'update');
    expect(validation.add).to.have.keys('body');
    expect(validation.update).to.have.keys('body');
  });
});
