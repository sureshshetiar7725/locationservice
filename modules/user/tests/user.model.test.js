import chai, { expect } from 'chai';
import sinon from 'sinon';
import userModel from '../user.model';

chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### user module **user.model.js**', () => {
  it('model should have add function', () => {
    expect(userModel.add).to.be.a('function');
  });
  it('add function should call create method internally', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', role: 'admin', email: 'xyz12@gmail.com' };
    sandbox.stub(userModel, 'create').resolves(userInfo);
    userModel.add(userInfo)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });
  it('add function should throw boom error', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', role: 'admin', email: 'xyz12@gmail.com' };
    sandbox.stub(userModel, 'create').rejects(new Error('some mongo error'));
    userModel.add(userInfo)
    .catch((error) => {
      expect(error.isBoom).to.be.eql(true);
      done();
    });
  });
// get function.

  it('model should have getUsers function', () => {
    expect(userModel.getUsers).to.be.a('function');
  });
  it('get function should call getUsers method internally', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', email: 'xyz12@gmail.com' };
    sandbox.stub(userModel, 'getUsers').resolves(userInfo);
    userModel.getUsers()
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });

// get by id.

  it('model should have findUserById function', () => {
    expect(userModel.findUserById).to.be.a('function');
  });
  it('get by id function should call findUserById method internally', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', role: 'admin', email: 'xyz12@gmail.com', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    sandbox.stub(userModel, 'findUserById').resolves(userInfo);
    userModel.findUserById(userInfo._id)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });
// find by id and update.

  it('model should have findUserByIdUpdate function', () => {
    expect(userModel.findUserByIdUpdate).to.be.a('function');
  });
  it('update by id function should call findUserByIdUpdate method internally', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', role: 'admin', email: 'xyz12@gmail.com', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    const userData = { name: 'camera' };
    sandbox.stub(userModel, 'findUserByIdUpdate').resolves(userInfo);
    userModel.findUserByIdUpdate(userInfo._id, userData.name)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });

// find by id and delete.

  it('model should have deleteUser function', () => {
    expect(userModel.deleteUser).to.be.a('function');
  });
  it('delete by id function should call deleteUser method internally', (done) => {
    const userInfo = { name: 'mobile', mobile: '8805664770', password: '12345678', role: 'admin', email: 'xyz12@gmail.com', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    sandbox.stub(userModel, 'deleteUser').resolves(userInfo);
    userModel.deleteUser(userInfo._id)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });
});
