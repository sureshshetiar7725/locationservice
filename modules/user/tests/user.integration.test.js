import mongoose from 'mongoose';
import jwt from 'jwt-simple';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../../index';
import config from '../../../config/database';
import User from '../user.model';
import userFixture from '../helpers/user.fixtures';

chai.config.includeStack = true;

/**
 * root level hooks
 */
after((done) => {
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

const userData = {
  _id: '5ab0c9d77f9ac31f4c7867d2',
  name: 'test',
  mobile: '9999999999',
  password: '12345',
  email: 'lmn@gmail.com',
};

let token;

// inserting data from fixtures into database.
beforeEach((done) => {
  token = jwt.encode(userData, config.secret);
  User.collection.insert(userFixture, done);
});

  // emptying database
afterEach((done) => {
  User.remove({}, () => {
    done();
  });
});

describe('## User APIs (integration test)', () => {
  const user = {
    name: 'shalu',
    mobile: '8457889562',
    password: '12345',
    email: 'testing@gmail.com',
    dob: '1996-06-07',
    gender: 'male',
  };
  const insufficientData = {
    name: 'test',
    mobile: '9999999999',
    password: '12345',
    email: 'lmn@gmail.com',
  };
  const validateUserObject = {
    name: 'test',
    mobile: 9999999999,
    password: '12345',
    email: 'lmn@gmail.com',
  };
// post
  describe('# POST /api/user', () => {
    it('should not create a user as token not provided', (done) => {
      request(app)
        .post('/api/user')
        .send(user)
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get bad request as data is insufficient', (done) => {
      request(app)
        .post('/api/user')
        .set('Authorization', `jwt ${token}`)
        .send(insufficientData)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get validation error', (done) => {
      request(app)
        .post('/api/user')
        .set('Authorization', `jwt ${token}`)
        .send(validateUserObject)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should  create a user', (done) => {
      request(app)
        .post('/api/user')
        .set('Authorization', `jwt ${token}`)
        .send(user)
        .expect(httpStatus.CREATED)
        .then((res) => {
          expect(res.body);
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('email');
          expect(res.body.data.name).to.equal(user.name);
          expect(res.body.data.mobile).to.equal(user.mobile);
          done();
        })
        .catch(done);
    });
  });
// get list
  describe('# GET /api/user', () => {
    it('should not get user list as token not provided', (done) => {
      request(app)
        .get('/api/user')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get user list', (done) => {
      request(app)
        .get('/api/user')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('users list found');
          expect(res.body.data).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });

// get by id
  describe('# GET BY ID /api/user/:id', () => {
    it('should not get user as token not provided', (done) => {
      request(app)
        .get('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get user by id', (done) => {
      request(app)
     .get('/api/user/4ed2b809d7446b9a0e000023')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user found');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('email');
          expect(res.body.data.name).to.equal('anand');
          done();
        })
        .catch(done);
    });

    it('should not get user as id doesnt exists', (done) => {
      request(app)
     .get('/api/user/4ed2b809d7446b9a0e000027')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });

// update
  describe('# UPDATE BY ID /api/user/:id', () => {
    it('should not update user as token not provided', (done) => {
      request(app)
        .put('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should update user by id', (done) => {
      request(app)
    .put('/api/user/4ed2b809d7446b9a0e000023')
        .send(user)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user updated');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('email');
          expect(res.body.data.name).to.equal(user.name);
          done();
        })
        .catch(done);
    });

    it('should get validation error', (done) => {
      request(app)
      .put('/api/user/4ed2b809d7446b9a0e000023')
        .send(validateUserObject)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body);
          expect(res.body).to.be.an('object');
          expect(res.body.message).to.equal('validation error');
          done();
        })
        .catch(done);
    });

    it('should not update user as id doesnt exists', (done) => {
      request(app)
     .put('/api/user/4ed2b809d7446b9a0e000027')
     .send(user)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });

// delete
  describe('# DELETE BY ID /api/user/:id', () => {
    it('should not delete user as token not provided', (done) => {
      request(app)
        .delete('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should delete user by id', (done) => {
      request(app)
  .delete('/api/user/4ed2b809d7446b9a0e000023')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user deleted');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('email');
          expect(res.body.data.name).to.equal('anand');
          done();
        })
        .catch(done);
    });

    it('should not delete user as id doesnt exists', (done) => {
      request(app)
     .delete('/api/user/4ed2b809d7446b9a0e000027')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });
});
