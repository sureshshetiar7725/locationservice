/* eslint-disable no-param-reassign */
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import beautifyUnique from 'mongoose-beautiful-unique-validation';
import response from '../../helpers/response';

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  dob: {
    type: Date,
  },
  gender: {
    type: String,
  },
  mobile: {
    type: String,
    required: false,
    unique: true,
    sparse: true,
  },
  password: {
    type: String,
  },
  email: {
    type: String,
    required: false,
    unique: true,
    sparse: true,
  },
},
  {
    timestamps: true,
    toObject: {
      transform(doc, ret) {
        delete ret.password;
      },
    },
    toJSON: {
      transform(doc, ret) {
        delete ret.password;
      },
    },
  });

UserSchema.plugin(beautifyUnique);

UserSchema.statics = {

  /**
   * add user
   * @param {String} user.name user name.
   * @param {String} user.mobile user mobile.
   * @param {String} user.password user password.
   * @param {String} user.email user email.
   * @param {String} user.dob user dob.
   * @param {String} user.gender user gender.
   * @returns {Promise<user, Error>} resolve added user.
   */

  add(data) {
    return this.create(data)
      .catch(error => response.ERROR.validateError(error));
  },

  getUsers() {
    return this.find()
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },

  /**
   * @param {String} user._id user id.
   * @returns {Promise<user, Error>} resolves finded user.
   */

  findUserById(_id) {
    return this.findById(_id)
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },

  /**
   * @param {String} user._id user id.
   * @returns {Promise<user, Error>} resolve updated user.
   */

  findUserByIdUpdate(_id, body) {
    return this.findByIdAndUpdate(_id, body, { new: true })
       .catch(
        /* istanbul ignore next */
        error => response.ERROR.validateError(error));
  },

  /**
   * @param {String} user._id user id.
   * @returns {Promise<user, Error>} resolve deleted user.
   */

  deleteUser(_id) {
    return this.findByIdAndRemove(_id)
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },
};

/* eslint-disable */
UserSchema.pre('save',
/* istanbul ignore next */
function (next) {
  var user = this;
  if (this.password){
  if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
          }
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err);
              }
            user.password = hash;
            next();
          });
      });
    } else {
      return next();
    }
  } else {
    next();
  }

});

UserSchema.pre('findOneAndUpdate',
/* istanbul ignore next */
function (next) {
  var user = this;
  if (user._update.password) {
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
          }
        bcrypt.hash(user._update.password, salt, function (err, hash) {
            if (err) {
                return next(err);
              }
            user._update.password = hash;
            next();
          });
      });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function (passw, cb) {
  bcrypt.compare(passw, this.password,
    /* istanbul ignore next */
    function (err, isMatch) {
    if (err) {
        return cb(err);
      }
    cb(null, isMatch);
  });
};

export default mongoose.model('user', UserSchema);
