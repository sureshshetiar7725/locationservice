/**
 * import all modules.
 */

import express from 'express';

import user from './user';

import auth from './auth';

import location from './locations';

/**
 * defining express route.
 */

const routes = express.Router(); // eslint-disable-line

routes.use('/user', user.route);

routes.use('/authenticate', auth.route);

routes.use('/locations', location.route);

export default {
  /**
   * modules export.
   */
  modules: {
    user,
    auth,
    location,
  },
  /**
   * express routes.
   */
  routes,
};
