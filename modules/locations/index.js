
/**
 * export all file to become consumable.
 * controller: controller of the module
 * model: model of the module
 * response: response of the module
 * route: route of the module
 * validation: validation of the module
 */


import controller from './locations.controller';
import model from './locations.model';
import route from './locations.route';
import validation from './locations.validation';

export default {
  controller,
  model,
  route,
  validation,
};
