
/**
 * Define controller function here.
 */

import Location from './locations.model';
import response from '../../helpers/response';
import googleApi from '../../helpers/googleMapApi';

const getList = [
  (req, res) => {
    Location.get()
        .then((result) => {
          if (result && result.length !== 0) {
            response.ok(res, { message: 'locations found', data: result });
          } else {
            response.noData(res, { message: 'no data found' });
          }
        })
        .catch(
          /* istanbul ignore next */
          (error) => {
            console.log('error logged', error);
            response.serverError(res, { message: 'Interval Server Error' });
          });
  },
];

const findById = [
  (req, res) => {
    Location.getById({ _id: req.params._id })
        .then((result) => {
          if (result) {
            response.ok(res, { message: 'location found', data: result });
          } else {
            response.noData(res, { message: 'no data found' });
          }
        })
        .catch(
          /* istanbul ignore next */
          (error) => {
            console.log('error logged', error);
            response.serverError(res, { message: 'Interval Server Error' });
          });
  },
];

const updateById = [
  (req, res) => {
    Location.getByIdAndUpdate({ _id: req.params._id }, req.body)
      .then((result) => {
        if (result) {
          response.ok(res, { message: 'location updated', data: result });
        } else {
          response.noData(res, { message: 'no data found' });
        }
      }).catch(
        /* istanbul ignore next */
        (error) => {
          console.log('error logged', error);
          response.serverError(res, { message: 'Interval Server Error' });
        });
  },
];

const deleteById = [
  (req, res) => {
    Location.deleteById({ _id: req.params._id })
     .then((result) => {
       if (result) {
         response.ok(res, { message: 'location deleted', data: result });
       } else {
         response.noData(res, { message: 'no data found' });
       }
     }).catch(
      /* istanbul ignore next */
      (error) => {
        console.log('error logged', error);
        response.serverError(res, { message: 'Interval Server Error' });
      });
  },
];

const getDataByGoogleApi = [
  (req, res, next) => {
    const lat = req.query.lat ? req.query.lat : '19.1668';
    const long = req.query.long ? req.query.long : '73.2368';
    const radius = req.query.radius ? req.query.radius : '1500';
    const type = req.query.type ? req.query.type : 'restaurant';
    const keyword = req.query.keyword ? req.query.keyword : '';
    const user = req.user._id;
    googleApi.locationData({ lat, long, radius, type, keyword, user })
    .then((result) => {
      if (result && result.length !== 0) {
        response.ok(res, { message: 'data found', data: result });
        req.locationdata = result; // eslint-disable-line no-param-reassign
        next();
      } else {
        response.noData(res, { message: 'no data found' });
      }
    })
    .catch(
      /* istanbul ignore next */
      (error) => {
        console.log('error logged', error);
        response.serverError(res, { message: 'Interval Server Error' });
      });
  },
  (req) => {
    Location.add(req.locationdata);
  },
];

export default {
  getList,
  findById,
  updateById,
  deleteById,
  getDataByGoogleApi,
};
