import mongoose from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';
import response from '../../helpers/response';

/**
 * location Schema
 */
const locationSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
  },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      default: 'Point',
    },
    coordinates: [Number],
  },
  name: {
    type: String,
  },
  place_id: {
    type: String,
  },
  price_level: {
    type: Number,
  },
  rating: {
    type: Number,
  },
  types: {
    type: Array,
  },
  vicinity: {
    type: String,
  },
  geometry: {
    type: Object,
  },
  user_ratings_total: {
    type: Number,
  },
},
  {
    timestamps: true,
  });

locationSchema.plugin(beautifyUnique);

locationSchema.statics = {

  add(data) {
    return this.create(data)
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.validateError(error));
  },

  get() {
    return this.find()
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },

  getById(_id) {
    return this.findById(_id)
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },

  getByIdAndUpdate(_id, body) {
    return this.findByIdAndUpdate(_id, body, { new: true })
       .catch(
        /* istanbul ignore next */
        error => response.ERROR.validateError(error));
  },

  deleteById(_id) {
    return this.findByIdAndRemove(_id)
      .catch(
        /* istanbul ignore next */
        error => response.ERROR.serverError(error));
  },
};

export default mongoose.model('locations', locationSchema);
