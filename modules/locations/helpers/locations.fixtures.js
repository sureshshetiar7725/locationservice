import objectId from 'objectid';

module.exports = [
  {
    _id: objectId('4ed2b809d7446b9a0e000031'),
    geometry: {
      location: {
        lat: 19.2412095,
        lng: 73.1455283,
      },
      viewport: {
        northeast: {
          lat: 19.24251297989273,
          lng: 73.14689302989272,
        },
        southwest: {
          lat: 19.23981332010728,
          lng: 73.14419337010727,
        },
      },
    },
    name: 'The Sai Leela Pavbhaji',
    place_id: 'ChIJOQpDai-U5zsRmnBdw_FsnMw',
    price_level: 2,
    rating: 3.8,
    types: [
      'restaurant',
      'point_of_interest',
      'food',
      'establishment',
    ],
    user_ratings_total: 53,
    vicinity: 'Murbad Kalyan Rd, Syndicate, Kalyan',
    location: {
      type: 'Point',
      coordinates: [
        73.1455283,
        19.2412095,
      ],
    },
    user: objectId('5ca5d23a417f220f4e1eb8fe'),
  },
];
