
/**
 * Define every validation requires
 * to perform CRUD on usefullPlaces.
 */

import Joi from 'joi';

// Defining base schema.
const locationSchema = {
  location: Joi.object({ coordinates: Joi.array().items(Joi.number()) }),
  name: Joi.string(),
  place_id: Joi.string(),
  price_level: Joi.number(),
  rating: Joi.number(),
  types: Joi.array(),
  vicinity: Joi.string(),
  geometry: Joi.object(),
  user_ratings_total: Joi.number(),
};


export default {
  // adding middleware level validation on api endpoint
  // add: {
  //   body: {
  //   },
  // },
  update: {
    body: {
      location: locationSchema.location,
      name: locationSchema.name,
      place_id: locationSchema.place_id,
      price_level: locationSchema.price_level,
      rating: locationSchema.rating,
      types: locationSchema.types,
      vicinity: locationSchema.vicinity,
      geometry: locationSchema.geometry,
      user_ratings_total: locationSchema.user_ratings_total,
    },
  },
};
