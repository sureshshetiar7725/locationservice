/**
 * Define your module's express routes here.
 */

import express from 'express';
import passport from 'passport';
import validator from 'express-validation';
import validation from './locations.validation';
import controller from './locations.controller';

const routes = express.Router(); // eslint-disable-line new-cap

/**
 * @api {get} /api/locations get list of locations from db.
 * @apiHeader {String} Authorization jwt token
 *
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName getLocations
 * @apiGroup Location
 *
 * @apiSuccess {Object} geometry geometry.
 * @apiSuccess {Object} geometry.location location.
 * @apiSuccess {Number} geometry.location.lat lat.
 * @apiSuccess {Number} geometry.location.lng lng.
 * @apiSuccess {Object} geometry.viewport viewport.
 * @apiSuccess {Object} geometry.viewport.northeast northeast.
 * @apiSuccess {Number} geometry.viewport.northeast.lat lat.
 * @apiSuccess {Number} geometry.viewport.northeast.lng lng.
 * @apiSuccess {Object} geometry.viewport.southwest southwest.
 * @apiSuccess {Number} geometry.viewport.southwest.lat lat.
 * @apiSuccess {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} place_id place_id.
 * @apiSuccess {Number} rating rating.
 * @apiSuccess {Array} types types.
 * @apiSuccess {Number} user_ratings_total user_ratings_total.
 * @apiSuccess {String} vicinity vicinity.
 *
 * @apiSuccess {Object} location location.
 * @apiSuccess {Object} location.type type.
 * @apiSuccess {Object} location.coordinates coordinates.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "locations found",
    "data": [
        {
            "_id": "5ca6296d8ac6fd4e279ec3d7",
            "updatedAt": "2019-04-04T15:57:33.556Z",
            "createdAt": "2019-04-04T15:57:33.556Z",
            "geometry": {
                "location": {
                    "lat": 19.2518458,
                    "lng": 73.1243908
                },
                "viewport": {
                    "northeast": {
                        "lat": 19.25323142989273,
                        "lng": 73.12573237989272
                    },
                    "southwest": {
                        "lat": 19.25053177010728,
                        "lng": 73.12303272010729
                    }
                }
            },
            "name": "Hotel Leela",
            "place_id": "ChIJQ3ye73mW5zsRIiOgKqFkL_g",
            "rating": 3.8,
            "user_ratings_total": 612,
            "vicinity": "Opp. Kalyan Sports Club, Adharwadi Chowk, Kalyan West, Thane",
            "user": "5ca5d23a417f220f4e1eb8fe",
            "__v": 0,
            "types": [
                "lodging",
                "restaurant",
                "point_of_interest",
                "food",
                "establishment"
            ],
            "location": {
                "coordinates": [
                    73.1243908,
                    19.2518458
                ],
                "type": "Point"
            }
        },
        {
            "_id": "5ca6296d8ac6fd4e279ec3d8",
            "updatedAt": "2019-04-04T15:57:33.571Z",
            "createdAt": "2019-04-04T15:57:33.571Z",
            "geometry": {
                "location": {
                    "lat": 19.2412095,
                    "lng": 73.1455283
                },
                "viewport": {
                    "northeast": {
                        "lat": 19.24251297989273,
                        "lng": 73.14689302989272
                    },
                    "southwest": {
                        "lat": 19.23981332010728,
                        "lng": 73.14419337010727
                    }
                }
            },
            "name": "The Sai Leela Pavbhaji",
            "place_id": "ChIJOQpDai-U5zsRmnBdw_FsnMw",
            "price_level": "2",
            "rating": 3.8,
            "user_ratings_total": 53,
            "vicinity": "Murbad Kalyan Rd, Syndicate, Kalyan",
            "user": "5ca5d23a417f220f4e1eb8fe",
            "__v": 0,
            "types": [
                "restaurant",
                "point_of_interest",
                "food",
                "establishment"
            ],
            "location": {
                "coordinates": [
                    73.1455283,
                    19.2412095
                ],
                "type": "Point"
            }
        }
    ]
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.get('/',
passport.authenticate('jwt', { session: false }),
controller.getList);

/**
 * @api {get} /api/locations/googleApi get locations from google api.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiDescription By default it will request for restaurants in badlapur in radius 1500 meter.
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/locations/googleApi?lat=19.1668&long=73.2368&radius=1500&type=restaurant&keyword=hotel
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName getLocationsFromGoogleApi
 * @apiGroup Location
 *
 * @apiSuccess {Object} geometry geometry.
 * @apiSuccess {Object} geometry.location location.
 * @apiSuccess {Number} geometry.location.lat lat.
 * @apiSuccess {Number} geometry.location.lng lng.
 * @apiSuccess {Object} geometry.viewport viewport.
 * @apiSuccess {Object} geometry.viewport.northeast northeast.
 * @apiSuccess {Number} geometry.viewport.northeast.lat lat.
 * @apiSuccess {Number} geometry.viewport.northeast.lng lng.
 * @apiSuccess {Object} geometry.viewport.southwest southwest.
 * @apiSuccess {Number} geometry.viewport.southwest.lat lat.
 * @apiSuccess {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} place_id place_id.
 * @apiSuccess {Number} rating rating.
 * @apiSuccess {Array} types types.
 * @apiSuccess {Number} user_ratings_total user_ratings_total.
 * @apiSuccess {String} vicinity vicinity.
 *
 * @apiSuccess {Object} location location.
 * @apiSuccess {Object} location.type type.
 * @apiSuccess {Object} location.coordinates coordinates.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "data found",
    "data": [
        {
            "geometry": {
                "location": {
                    "lat": 19.2518458,
                    "lng": 73.1243908
                },
                "viewport": {
                    "northeast": {
                        "lat": 19.25323142989273,
                        "lng": 73.12573237989272
                    },
                    "southwest": {
                        "lat": 19.25053177010728,
                        "lng": 73.12303272010729
                    }
                }
            },
            "name": "Hotel Leela",
            "place_id": "ChIJQ3ye73mW5zsRIiOgKqFkL_g",
            "rating": 3.8,
            "types": [
                "lodging",
                "restaurant",
                "point_of_interest",
                "food",
                "establishment"
            ],
            "user_ratings_total": 612,
            "vicinity": "Opp. Kalyan Sports Club, Adharwadi Chowk, Kalyan West, Thane",
            "location": {
                "type": "Point",
                "coordinates": [
                    73.1243908,
                    19.2518458
                ]
            },
            "user": "5ca5d23a417f220f4e1eb8fe"
        },
        {
            "geometry": {
                "location": {
                    "lat": 19.2412095,
                    "lng": 73.1455283
                },
                "viewport": {
                    "northeast": {
                        "lat": 19.24251297989273,
                        "lng": 73.14689302989272
                    },
                    "southwest": {
                        "lat": 19.23981332010728,
                        "lng": 73.14419337010727
                    }
                }
            },
            "name": "The Sai Leela Pavbhaji",
            "place_id": "ChIJOQpDai-U5zsRmnBdw_FsnMw",
            "price_level": 2,
            "rating": 3.8,
            "types": [
                "restaurant",
                "point_of_interest",
                "food",
                "establishment"
            ],
            "user_ratings_total": 53,
            "vicinity": "Murbad Kalyan Rd, Syndicate, Kalyan",
            "location": {
                "type": "Point",
                "coordinates": [
                    73.1455283,
                    19.2412095
                ]
            },
            "user": "5ca5d23a417f220f4e1eb8fe"
        }
    ]
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.get('/googleApi',
passport.authenticate('jwt', { session: false }),
controller.getDataByGoogleApi);

/**
 * @api {get} /api/locations/:_id get location by id.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/locations/5ca6296d8ac6fd4e279ec3d7
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName getById
 * @apiGroup Location
 *
 * @apiSuccess {Object} geometry geometry.
 * @apiSuccess {Object} geometry.location location.
 * @apiSuccess {Number} geometry.location.lat lat.
 * @apiSuccess {Number} geometry.location.lng lng.
 * @apiSuccess {Object} geometry.viewport viewport.
 * @apiSuccess {Object} geometry.viewport.northeast northeast.
 * @apiSuccess {Number} geometry.viewport.northeast.lat lat.
 * @apiSuccess {Number} geometry.viewport.northeast.lng lng.
 * @apiSuccess {Object} geometry.viewport.southwest southwest.
 * @apiSuccess {Number} geometry.viewport.southwest.lat lat.
 * @apiSuccess {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} place_id place_id.
 * @apiSuccess {Number} rating rating.
 * @apiSuccess {Array} types types.
 * @apiSuccess {Number} user_ratings_total user_ratings_total.
 * @apiSuccess {String} vicinity vicinity.
 *
 * @apiSuccess {Object} location location.
 * @apiSuccess {Object} location.type type.
 * @apiSuccess {Object} location.coordinates coordinates.
 * @apiSuccessExample {json} Success.
 *
 *  {
    "statusCode": 200,
    "message": "location found",
    "data": {
        "_id": "5ca6296d8ac6fd4e279ec3d7",
        "updatedAt": "2019-04-04T15:57:33.556Z",
        "createdAt": "2019-04-04T15:57:33.556Z",
        "geometry": {
            "location": {
                "lat": 19.2518458,
                "lng": 73.1243908
            },
            "viewport": {
                "northeast": {
                    "lat": 19.25323142989273,
                    "lng": 73.12573237989272
                },
                "southwest": {
                    "lat": 19.25053177010728,
                    "lng": 73.12303272010729
                }
            }
        },
        "name": "Hotel Leela",
        "place_id": "ChIJQ3ye73mW5zsRIiOgKqFkL_g",
        "rating": 3.8,
        "user_ratings_total": 612,
        "vicinity": "Opp. Kalyan Sports Club, Adharwadi Chowk, Kalyan West, Thane",
        "user": "5ca5d23a417f220f4e1eb8fe",
        "__v": 0,
        "types": [
            "lodging",
            "restaurant",
            "point_of_interest",
            "food",
            "establishment"
        ],
        "location": {
            "coordinates": [
                73.1243908,
                19.2518458
            ],
            "type": "Point"
        }
    }
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.get('/:_id',
passport.authenticate('jwt', { session: false }),
controller.findById);

/**
 * @api {put} /api/locations/:_id update location.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/locations/5ca6296d8ac6fd4e279ec3d7
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName updateById
 * @apiGroup Location
 *
 * @apiParam {Object} geometry geometry.
 * @apiParam {Object} geometry.location location.
 * @apiParam {Number} geometry.location.lat lat.
 * @apiParam {Number} geometry.location.lng lng.
 * @apiParam {Object} geometry.viewport viewport.
 * @apiParam {Object} geometry.viewport.northeast northeast.
 * @apiParam {Number} geometry.viewport.northeast.lat lat.
 * @apiParam {Number} geometry.viewport.northeast.lng lng.
 * @apiParam {Object} geometry.viewport.southwest southwest.
 * @apiParam {Number} geometry.viewport.southwest.lat lat.
 * @apiParam {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiParam {String} name name.
 * @apiParam {String} place_id place_id.
 * @apiParam {Number} rating rating.
 * @apiParam {Array} types types.
 * @apiParam {Number} user_ratings_total user_ratings_total.
 * @apiParam {String} vicinity vicinity.
 *
 * @apiParam {Object} location location.
 * @apiParam {Object} location.type type.
 * @apiParam {Object} location.coordinates coordinates.
 *
 * @apiParamExample {json} Body.
 * {
  "geometry": {
    "location": {
      "lat": 19.2412095,
      "lng": 73.1455283
    },
    "viewport": {
      "northeast": {
        "lat": 19.24251297989273,
        "lng": 73.14689302989272
      },
      "southwest": {
        "lat": 19.23981332010728,
        "lng": 73.14419337010727
      }
    }
  },
  "name": "The Sai Leela Pavbhaji",
  "place_id": "ChIJOQpDai-U5zsRmnBdw_FsnMw",
  "price_level": 2,
  "rating": 3.8,
  "types": [
    "restaurant",
    "point_of_interest",
    "food",
    "establishment"
  ],
  "user_ratings_total": 53,
  "vicinity": "Murbad Kalyan Rd, Syndicate, Kalyan",
  "location": {
    "type": "Point",
    "coordinates": [
      73.1455283,
      19.2412095
    ]
  },
  "user": "5ca5d23a417f220f4e1eb8fe"
}
 * @apiSuccess {Object} geometry geometry.
 * @apiSuccess {Object} geometry.location location.
 * @apiSuccess {Number} geometry.location.lat lat.
 * @apiSuccess {Number} geometry.location.lng lng.
 * @apiSuccess {Object} geometry.viewport viewport.
 * @apiSuccess {Object} geometry.viewport.northeast northeast.
 * @apiSuccess {Number} geometry.viewport.northeast.lat lat.
 * @apiSuccess {Number} geometry.viewport.northeast.lng lng.
 * @apiSuccess {Object} geometry.viewport.southwest southwest.
 * @apiSuccess {Number} geometry.viewport.southwest.lat lat.
 * @apiSuccess {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} place_id place_id.
 * @apiSuccess {Number} rating rating.
 * @apiSuccess {Array} types types.
 * @apiSuccess {Number} user_ratings_total user_ratings_total.
 * @apiSuccess {String} vicinity vicinity.
 *
 * @apiSuccess {Object} location location.
 * @apiSuccess {Object} location.type type.
 * @apiSuccess {Object} location.coordinates coordinates.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "location updated",
    "data": {
        "_id": "5ca6296d8ac6fd4e279ec3d7",
        "updatedAt": "2019-04-04T16:10:18.345Z",
        "createdAt": "2019-04-04T15:57:33.556Z",
        "geometry": {
            "location": {
                "lat": 19.2412095,
                "lng": 73.1455283
            },
            "viewport": {
                "northeast": {
                    "lat": 19.24251297989273,
                    "lng": 73.14689302989272
                },
                "southwest": {
                    "lat": 19.23981332010728,
                    "lng": 73.14419337010727
                }
            }
        },
        "name": "The Sai Leela Pavbhaji",
        "place_id": "ChIJOQpDai-U5zsRmnBdw_FsnMw",
        "rating": 3.8,
        "user_ratings_total": 53,
        "vicinity": "Murbad Kalyan Rd, Syndicate, Kalyan",
        "user": "5ca5d23a417f220f4e1eb8fe",
        "__v": 0,
        "price_level": 2,
        "types": [
            "restaurant",
            "point_of_interest",
            "food",
            "establishment"
        ],
        "location": {
            "coordinates": [
                73.1455283,
                19.2412095
            ],
            "type": "Point"
        }
    }
}
 *
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.put('/:_id',
passport.authenticate('jwt', { session: false }),
validator(validation.update),
controller.updateById);

/**
 * @api {delete} /api/locations/:_id delete location by id.
 * @apiHeader {String} Authorization jwt token
 *
 * @apiExample {curl} urlExample:
 * http://localhost:4047/api/locations/5ca6296d8ac6fd4e279ec3d7
 *
 * @apiHeaderExample {json} Header-example
 * {
 "Autorization": "jwt eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ8.eyJfaWQiOiI1YWFmNzhlZ
 WE0MzBjPjE5ODkwOGU8ZDkiLCJ1cGRhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg6MVoiLCJj
 cmVhdGVkQXQiOiIyMDE4LTAzLTE5VDA4OjQ2OjM4Ljg3MVoiLCJuYW1lIjoic3VyZXNoIiwibW9iaWxl
 IjoiODgwNTY2NDc3MCIsInJvbGUiOiJhZG1pbiIsIl9fdiI6MH0.Dz_8pmChieSnu9YfKB_ZvA5oWtO
 -GIbdSc6Pl6q3uN4"
 }
 *
 * @apiVersion 0.0.1
 * @apiName deleteById
 * @apiGroup Location
 *
 * @apiSuccess {Object} geometry geometry.
 * @apiSuccess {Object} geometry.location location.
 * @apiSuccess {Number} geometry.location.lat lat.
 * @apiSuccess {Number} geometry.location.lng lng.
 * @apiSuccess {Object} geometry.viewport viewport.
 * @apiSuccess {Object} geometry.viewport.northeast northeast.
 * @apiSuccess {Number} geometry.viewport.northeast.lat lat.
 * @apiSuccess {Number} geometry.viewport.northeast.lng lng.
 * @apiSuccess {Object} geometry.viewport.southwest southwest.
 * @apiSuccess {Number} geometry.viewport.southwest.lat lat.
 * @apiSuccess {Number} geometry.viewport.southwest.lng lng.
 *
 * @apiSuccess {String} name name.
 * @apiSuccess {String} place_id place_id.
 * @apiSuccess {Number} rating rating.
 * @apiSuccess {Array} types types.
 * @apiSuccess {Number} user_ratings_total user_ratings_total.
 * @apiSuccess {String} vicinity vicinity.
 *
 * @apiSuccess {Object} location location.
 * @apiSuccess {Object} location.type type.
 * @apiSuccess {Object} location.coordinates coordinates.
 * @apiSuccessExample {json} Success.
 *
 *   {
    "statusCode": 200,
    "message": "location deleted",
    "data": {
        "_id": "5ca6296d8ac6fd4e279ec3d7",
        "updatedAt": "2019-04-04T16:10:18.345Z",
        "createdAt": "2019-04-04T15:57:33.556Z",
        "geometry": {
            "location": {
                "lat": 19.2412095,
                "lng": 73.1455283
            },
            "viewport": {
                "northeast": {
                    "lat": 19.24251297989273,
                    "lng": 73.14689302989272
                },
                "southwest": {
                    "lat": 19.23981332010728,
                    "lng": 73.14419337010727
                }
            }
        },
        "name": "The Sai Leela Pavbhaji",
        "place_id": "ChIJOQpDai-U5zsRmnBdw_FsnMw",
        "rating": 3.8,
        "user_ratings_total": 53,
        "vicinity": "Murbad Kalyan Rd, Syndicate, Kalyan",
        "user": "5ca5d23a417f220f4e1eb8fe",
        "__v": 0,
        "price_level": 2,
        "types": [
            "restaurant",
            "point_of_interest",
            "food",
            "establishment"
        ],
        "location": {
            "coordinates": [
                73.1455283,
                19.2412095
            ],
            "type": "Point"
        }
    }
}
 *
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "message": "No data found"
}
 * @apiErrorExample {json} unAuthorized.
 * {
    "statusCode": 403,
    "message": "unAuthorized"
}
*
*/

routes.delete('/:_id',
passport.authenticate('jwt', { session: false }),
controller.deleteById);

export default routes;
