import chai, { expect } from 'chai';
import index from '../index';

chai.config.includeStack = true;

describe('### locations module **index.js**', () => {
  it('should has controller, route, model, response and validation property', () => {
    expect(index).to.have.keys('controller', 'route', 'model', 'validation');
  });
});
