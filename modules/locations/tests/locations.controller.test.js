import chai, { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'proxyquire';
import locationModel from '../locations.model';

// stubbing user dependency from user.controller.
const LocationController = proxyquire('../locations.controller', { User: locationModel });


chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### locations controller **locations.controller.js**', () => {
  it('should have getList property', () => {
    expect(LocationController).to.be.a('object');
    expect(LocationController.getList).to.be.a('array');
  });
  it('should have findById property', () => {
    expect(LocationController).to.be.a('object');
    expect(LocationController.findById).to.be.a('array');
  });
  it('should have updateById property', () => {
    expect(LocationController).to.be.a('object');
    expect(LocationController.updateById).to.be.a('array');
  });
  it('should have deleteById property', () => {
    expect(LocationController).to.be.a('object');
    expect(LocationController.deleteById).to.be.a('array');
  });
  it('should have getDataByGoogleApi property', () => {
    expect(LocationController).to.be.a('object');
    expect(LocationController.getDataByGoogleApi).to.be.a('array');
  });
});
