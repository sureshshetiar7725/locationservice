import chai, { expect } from 'chai';
import sinon from 'sinon';
import locationModel from '../locations.model';

chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### locations module **locations.model.js**', () => {
// get function.
  it('model should have get function', () => {
    expect(locationModel.get).to.be.a('function');
  });
  it('get function should call get method internally', (done) => {
    const userInfo = { name: 'suresh' };
    sandbox.stub(locationModel, 'get').resolves(userInfo);
    locationModel.get()
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });

// get by id.

  it('model should have getById function', () => {
    expect(locationModel.getById).to.be.a('function');
  });
  it('get by id function should call getById method internally', (done) => {
    const userInfo = { name: 'suresh', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    sandbox.stub(locationModel, 'getById').resolves(userInfo);
    locationModel.getById(userInfo._id)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });
// find by id and update.

  it('model should have getByIdAndUpdate function', () => {
    expect(locationModel.getByIdAndUpdate).to.be.a('function');
  });
  it('update by id function should call getByIdAndUpdate method internally', (done) => {
    const userInfo = { name: 'suresh', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    const userData = { name: 'camera' };
    sandbox.stub(locationModel, 'getByIdAndUpdate').resolves(userInfo);
    locationModel.getByIdAndUpdate(userInfo._id, userData.name)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });

// find by id and delete.

  it('model should have deleteById function', () => {
    expect(locationModel.deleteById).to.be.a('function');
  });
  it('delete by id function should call deleteById method internally', (done) => {
    const userInfo = { name: 'suresh', _id: '5a8fe561c3b0d22bf2d5ea8f' };
    sandbox.stub(locationModel, 'deleteById').resolves(userInfo);
    locationModel.deleteById(userInfo._id)
    .then((result) => {
      expect(result).to.be.deep.equal(userInfo);
      done();
    });
  });
});
