import mongoose from 'mongoose';
import jwt from 'jwt-simple';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../../index';
import config from '../../../config/database';
import Location from '../locations.model';
import locationFixture from '../helpers/locations.fixtures';

chai.config.includeStack = true;

/**
 * root level hooks
 */
after((done) => {
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

const userData = {
  _id: '5ab0c9d77f9ac31f4c7867d2',
  name: 'test',
  mobile: '9999999999',
  password: '12345',
  email: 'lmn@gmail.com',
};

let token;

// inserting data from fixtures into database.
beforeEach((done) => {
  token = jwt.encode(userData, config.secret);
  Location.collection.insert(locationFixture, done);
});

  // emptying database
afterEach((done) => {
  Location.remove({}, () => {
    done();
  });
});

describe('## Location APIs (integration test)', () => {
  const locationData = {
    name: 'The Sai Leela Pavbhaji',
    place_id: 'ChIJOQpDai-U5zsRmnBdw_FsnMw',
    price_level: 2,
    rating: 3.8,
  };
// get list
  describe('# GET /api/locations', () => {
    it('should not get locations list as token not provided', (done) => {
      request(app)
        .get('/api/locations')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get locations list', (done) => {
      request(app)
        .get('/api/locations')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('locations found');
          expect(res.body.data).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });

// get by id
  describe('# GET BY ID /api/locations/:id', () => {
    it('should not get location as token not provided', (done) => {
      request(app)
        .get('/api/locations/4ed2b809d7446b9a0e000031')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get location by id', (done) => {
      request(app)
     .get('/api/locations/4ed2b809d7446b9a0e000031')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('location found');
          done();
        })
        .catch(done);
    });

    it('should not get location as id doesnt exists', (done) => {
      request(app)
     .get('/api/locations/4ed2b809d7446b9a0e000027')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });

// update
  describe('# UPDATE BY ID /api/locations/:id', () => {
    it('should not update location as token not provided', (done) => {
      request(app)
        .put('/api/locations/4ed2b809d7446b9a0e000031')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should update location by id', (done) => {
      request(app)
    .put('/api/locations/4ed2b809d7446b9a0e000031')
        .send(locationData)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('location updated');
          done();
        })
        .catch(done);
    });

    it('should not update location as id doesnt exists', (done) => {
      request(app)
     .put('/api/locations/4ed2b809d7446b9a0e000027')
     .send(locationData)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });

// delete
  describe('# DELETE BY ID /api/locations/:id', () => {
    it('should not delete location as token not provided', (done) => {
      request(app)
        .delete('/api/locations/4ed2b809d7446b9a0e000031')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should delete location by id', (done) => {
      request(app)
  .delete('/api/locations/4ed2b809d7446b9a0e000031')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.data).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('location deleted');
          done();
        })
        .catch(done);
    });

    it('should not delete location as id doesnt exists', (done) => {
      request(app)
     .delete('/api/locations/4ed2b809d7446b9a0e000027')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body).to.be.an('object');
          done();
        })
        .catch(done);
    });
  });
// get list from google api
  describe('# GET /api/locations/googleApi', () => {
    it('should not get locations list from google api as token not provided', (done) => {
      request(app)
        .get('/api/locations/googleApi')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get locations list from google api', (done) => {
      request(app)
        .get('/api/locations/googleApi')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('data found');
          expect(res.body.data).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });
});
