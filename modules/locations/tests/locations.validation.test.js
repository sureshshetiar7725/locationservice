import chai, { expect } from 'chai';
import validation from '../locations.validation';

chai.config.includeStack = true;

describe('### locations module **locations.validation.js**', () => {
  it('should have add property with body', () => {
    expect(validation).to.have.keys('update');
    expect(validation.update).to.have.keys('body');
  });
});
